import pygame, os, math
from settings import *

#Settings
#moved to the settings file instead

WINDOW = pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption("Tile Game")
BACKGROUND = pygame.transform.scale(
    pygame.image.load(
        os.path.join('Assets','GroundTiles','grass.png')
    ).convert_alpha() , (WIDTH,HEIGHT)
)
LUMBRIDGE = pygame.image.load(
        os.path.join('Assets','GroundTiles','lumbridge_map.png')
    ).convert_alpha()

         
#Colors 
WHITE = (255,255,255)
BLACK = (0,0,0)

#Classes
    
class player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.transform.rotozoom(
            pygame.image.load(
                os.path.join('Assets','Character','player.png')
            ).convert_alpha(), 0, PLAYER_SIZE
        )
        
        self.position = pygame.math.Vector2(PLAYER_START_X,PLAYER_START_Y)
        self.speed = PLAYER_SPEED
        self.rect = self.image.get_rect(center=self.position)
        
        
    def user_input(self):
        self.velocity_x = 0
        self.velocity_y = 0
        
        keys = pygame.key.get_pressed()
        
        if (keys[pygame.K_a]): #left
            self.velocity_x = -self.speed
        if keys[pygame.K_d]: # right
            self.velocity_x = self.speed
        if keys[pygame.K_w]: # up
            self.velocity_y = -self.speed
        if keys[pygame.K_s]: # down
            self.velocity_y = self.speed
            
    def move(self):
        self.position += pygame.math.Vector2(self.velocity_x,self.velocity_y)
        self.rect.center = self.position
        
    def update(self):
        self.user_input()
        self.move()
    
class Camera(pygame.sprite.Group):
    def __init__(self):
        super().__init__()
        self.offset = pygame.math.Vector2()
        self.floor_rect = LUMBRIDGE.get_rect(topleft = (-2000,-2900))

    def custom_draw(self,all_sprite_group,hero): 
        self.offset.x = hero.rect.centerx - WIDTH // 2
        self.offset.y = hero.rect.centery - HEIGHT // 2

        floor_offset_pos = self.floor_rect.topleft - self.offset
        WINDOW.blit(LUMBRIDGE,floor_offset_pos)

        for sprite in all_sprite_group:
            offset_pos = sprite.rect.topleft - self.offset
            WINDOW.blit(sprite.image,offset_pos)

class Node(pygame.sprite.Group):
    def __init__(self, node_type, position_x, position_y):
        super().__init__()
        self.node_type = node_type
        self.position = pygame.math.Vector2(position_x,position_y)
        self.image = pygame.transform.rotozoom(
                pygame.image.load(
            os.path.join('Assets','Nodes', (self.node_type + ".png"))
        ).convert_alpha(), 0, 1)

def main():
    
    all_sprite_group = pygame.sprite.Group()
    
    camera = Camera()
    hero = player()
    normal_tree = Node("normal_tree",-2050,-2950)

    all_sprite_group.add(hero,normal_tree)
    clock = pygame.time.Clock()
    run = True
    
    while run: 
        
        keys = pygame.key.get_pressed()
        
        for  event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        all_sprite_group.update()

        camera.custom_draw(all_sprite_group,hero)

        pygame.display.update()
        clock.tick(FPS)

    pygame.quit()

if __name__ == "__main__":
    main()